# MeoTV #

MeoTv Application - This app help a Meo's clients to choice what you wanna see on TV.
In each channel is the user can see which current program and the following

### Project ###

This project follow the MVP Architecture

The main libraries used was:

* Retrofit
* RxAndroid
* Glide

## Flow ##

Managing new items to load is done by reyclerView in the onbindviewholder method, but the loaded data is saved as cache.

On reyclerView is scrolled near the end, a new request is made to the API, resulting in new channels and the link to the next request. This is done until all the API content is loaded.

For each channel of the grid retrieved from the API two requests are made:

- get information about the channel (this will update the cache)
- get the image (The image cache is managed by Glide)

### UX Features ###

* Pull to up to refresh
* Pull to down toload more content
* Press back to restore scroll to top