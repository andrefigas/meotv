package labs.altice.meotv.view

import android.support.v7.widget.RecyclerView
import labs.altice.meotv.model.entities.Channel

interface AdapterView<M : RecyclerView.ViewHolder, G : RecyclerView.ViewHolder, I> {

    fun bindMainHolder(holder: M, channel: I?)
    fun bindGenericHolder(holder: G)
}