package labs.altice.meotv.view

import android.content.Context
import android.graphics.Bitmap

interface ChannelsHolderView {

    fun showChannelTitle(titleText: String)

    fun showChannelTitleFailure()

    fun showImageProgressPlaceholder()

    fun showImageFailure()

    fun showImageSuccess(bitmap: Bitmap)

    fun showNowProgramSuccess(nowText: String)

    fun showNowProgramFailure()

    fun showNextProgramSuccess(nextText: String)

    fun showNextProgramFailure()

    fun getContext(): Context

    fun refreshProgramProgress(progressing: Boolean)
}