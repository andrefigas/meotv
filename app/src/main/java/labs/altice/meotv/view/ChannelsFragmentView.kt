package labs.altice.meotv.view

interface ChannelsFragmentView {

    fun refreshProgramProgress(progressing: Boolean)

    fun refresh()

    fun isAvailableView() : Boolean

    fun isTopCardVisible() : Boolean

    fun scrollToTop()

}