package labs.altice.meotv.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun Context.getSupportDrawable(@DrawableRes idRes: Int): Drawable {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        resources.getDrawable(idRes, theme)
    } else {
        resources.getDrawable(idRes)
    }
}

fun RecyclerView.Adapter<*>.getView(@LayoutRes idRes: Int, inflater: LayoutInflater, parent: ViewGroup?): View {
    return inflater.inflate(idRes, parent, false)
}

fun RecyclerView.refresh() {
    adapter = adapter
}