package labs.altice.meotv.model.controllers

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import labs.altice.meotv.model.CHANNELS
import labs.altice.meotv.model.api.ApiControllerProvider
import labs.altice.meotv.model.api.EndPoint
import labs.altice.meotv.model.entities.ChannelsGrid
import retrofit2.Response
import java.net.URLDecoder


class ChannelsGridAPIController(val skip: Int = 0) {

    companion object {
        private var instance: ChannelsGridAPIController? = null
        fun newIntance(skip: Int): ChannelsGridAPIController {
            safe()
            val newInstance = ChannelsGridAPIController(skip)
            instance = newInstance
            return newInstance
        }

        fun safe() {
            val controller = instance
            if (controller == null) {
                return
            }
            if (controller.disposable.isDisposed) {
                controller.disposable.dispose()
            }
        }
    }

    val observable: Observable<Response<ChannelsGrid>>
    lateinit var disposable: Disposable

    init {
        observable = ApiControllerProvider.getIntance().provideService(CHANNELS).getChannels(EndPoint.USER_AGENT,
                URLDecoder.decode(EndPoint.FILTER_CHANNELS, "UTF-8"), EndPoint.ORDER, EndPoint.INLINE_COUNT, skip)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun request(onSuccess: Consumer<Response<ChannelsGrid>>, onFailure: Consumer<Throwable>, onFinish: Action) {
        disposable = observable?.doOnDispose(onFinish)?.subscribe(onSuccess, onFailure) as Disposable
    }

}
