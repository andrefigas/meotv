package labs.altice.meotv.model.api;

import io.reactivex.Observable;
import labs.altice.meotv.BuildConfig;
import labs.altice.meotv.model.api.parse.ProgramsGridParse;
import labs.altice.meotv.model.entities.ChannelsGrid;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/*
* Was used .java class because jave cannot use
* a buildConfig like as constant
*/


public interface EndPoint {

    String USER_AGENT = "AND";
    String FILTER_CHANNELS = "substringof('MEO_Mobile',AvailableOnChannels) and IsAdult eq false";
    String FILTER_PROGRAMS = "CallLetter eq '%s'";
    String ORDER = "ChannelPosition asc";
    String INLINE_COUNT = "allpages";


    @GET(BuildConfig.ROUTE_API + "catalog/v6/Channels")
    Observable<Response<ChannelsGrid>> getChannels(@Query("UserAgent") String userAgent, @Query("$filter") String filter,
                                                   @Query("$orderby") String orderBy, @Query("$inlinecount") String inlineCount,
                                                   @Query("$skip") int skip);

    @GET(BuildConfig.ROUTE_API + "Program/v6/Programs/NowAndNextLiveChannelPrograms")
    Observable<Response<ProgramsGridParse>> getPrograms(@Query("UserAgent") String userAgent,
                                                        @Query("$filter") String filter);

}
