package labs.altice.meotv.model.api

import labs.altice.meotv.model.Api
import labs.altice.meotv.model.api.retrofit.RetrofitProvider
import okhttp3.Headers
import java.lang.NullPointerException
import java.util.regex.Pattern

const val CACHE_CONTROL = "cache-control"
const val CACHE_AGE = "max-age"

class ApiControllerImpl : ApiController {

    val cacheTimeMap = mutableMapOf<Long, Long>()
    val endpointMap = mutableMapOf<Long, EndPoint>()

    override fun setupCache(@Api api: Long, headers: Headers?) {

        if (cacheTimeMap.containsKey(api) || headers == null || headers.get(CACHE_CONTROL) == null) {
            return
        }

        val pattern = Pattern.compile("${CACHE_AGE}=([0-9]*)")
        val matcher = pattern.matcher(headers.get(CACHE_CONTROL))
        if (matcher.find()) {
            try {
                val cacheLengh = Integer.valueOf(matcher.group(1)).toLong()
                cacheTimeMap.put(api, cacheLengh)
                endpointMap.put(api, RetrofitProvider.getIntance().provideService(cacheLengh).create(EndPoint::class.java))
            } catch (e: NullPointerException) {
                e.printStackTrace();
            }

        }

    }

    override fun provideService(@Api api: Long): EndPoint {
        if (cacheTimeMap.containsKey(api)) {
            return endpointMap.get(api) as EndPoint
        } else {
            return RetrofitProvider.getIntance().provideService().create(EndPoint::class.java)
        }

    }

}