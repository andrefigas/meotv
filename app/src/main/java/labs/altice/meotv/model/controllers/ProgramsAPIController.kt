package labs.altice.meotv.model.controllers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import labs.altice.meotv.model.PROGRAMS
import labs.altice.meotv.model.api.ApiControllerProvider
import labs.altice.meotv.model.api.EndPoint
import labs.altice.meotv.model.api.parse.ProgramsGridParse
import retrofit2.Response
import java.net.URLDecoder


object ProgramsAPIController {

    fun request(channelId: String, onSuccess: Consumer<Response<ProgramsGridParse>>, onFailure: Consumer<Throwable>) {
        ApiControllerProvider.getIntance().provideService(PROGRAMS).getPrograms(EndPoint.USER_AGENT,
                URLDecoder.decode(String.format(EndPoint.FILTER_PROGRAMS, channelId), "UTF-8"))
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(object : ObservableSource<Response<ProgramsGridParse>> {
                    override fun subscribe(observer: Observer<in Response<ProgramsGridParse>>) {
                        onFailure.accept(Throwable(channelId))
                    }

                })
                .subscribeOn(Schedulers.io()).subscribe(onSuccess)
    }

    fun request(context: Context, id: String, url: String, onSuccess: Consumer<Pair<String, Bitmap>>,
                onFailure: Consumer<Pair<String, Exception?>>) {
        Glide.with(context)
                .load(url)
                .asBitmap().into<SimpleTarget<Bitmap>>(object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                if (resource != null) {
                    onSuccess.accept(Pair(id, resource))
                } else {
                    onFailure.accept(Pair<String, Exception?>(id, null))
                }
            }

            override fun onLoadFailed(e: Exception?, errorDrawable: Drawable?) {
                super.onLoadFailed(e, errorDrawable)
                onFailure.accept(Pair(id, e))
            }
        })
    }

}
