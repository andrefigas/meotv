package labs.altice.meotv.model.entities

import com.google.gson.annotations.SerializedName

const val PROGRAM_FIELD_TITLE: String = "Title"
const val PROGRAM_FIELD_TITLE_ID: String = "TitleId"

open class Program(@JvmField @SerializedName(PROGRAM_FIELD_TITLE) val title: String?,
                   @JvmField @SerializedName(PROGRAM_FIELD_TITLE_ID) val titleId: String?)