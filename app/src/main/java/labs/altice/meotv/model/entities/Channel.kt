package labs.altice.meotv.model.entities

import com.google.gson.annotations.SerializedName

const val CHANNEL_FIELD_TITLE: String = "Title"
const val CHANNEL_FIELD_CHANNEL_ID: String = "CallLetter"

data class Channel(@JvmField
                   @SerializedName
                   (CHANNEL_FIELD_TITLE) var title: String? = null,
                   @JvmField
                   @SerializedName
                   (CHANNEL_FIELD_CHANNEL_ID) var channelId: String? = null,
                   var now: Program? = null,
                   var next: Program? = null,
                   var thumbnail: String? = null) {

    override fun equals(other: Any?): Boolean {
        if (other is String && channelId != null && other == channelId) {
            return true
        }else  if (other is Channel && channelId != null && other.channelId == channelId) {
            return true
        }

        return super.equals(other)
    }
}