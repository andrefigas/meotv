package labs.altice.meotv.model

import android.support.annotation.IntDef

const val CHANNELS = 0L
const val PROGRAMS = 1L

@IntDef(CHANNELS, PROGRAMS)
@Retention(AnnotationRetention.SOURCE)
annotation class Api