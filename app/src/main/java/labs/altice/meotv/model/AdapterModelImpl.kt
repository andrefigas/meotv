package labs.altice.meotv.model

import labs.altice.meotv.model.contracts.AdapterModel
import labs.altice.meotv.model.entities.Channel
import labs.altice.meotv.presenter.contracts.AdapterPresenter
import kotlin.properties.Delegates

class AdapterModelImpl(val prsenter: AdapterPresenter) : AdapterModel<Channel> {

    override var skip: Int = 0
    override var loadMore: Boolean = true
    override var loadingMore: Boolean = false
    override var recycling: Boolean = false
    var pendingChannels = mutableListOf<String>()
    var cachedChannels = mutableListOf<String>()

    var channels: Array<Channel> by Delegates.observable(emptyArray<Channel>()) { prop, old, new ->
        if (state == READY) {
            //state no changed, just data was incremented
            prsenter.onIncremeted(old.size - 1, new.size - old.size)
        } else if (!recycling) {
            //new data received
            state = if (channels.size == 0) EMPTY else READY
        }

    }

    var state: Long by Delegates.observable(STARTING, { prop, old, new ->
        {
            if (new == RECYCLING) {
                //clear session data
                pendingChannels.clear()
                cachedChannels.clear()
                skip = 0
                loadMore = true
                loadingMore = false
                channels = emptyArray()
            }

            prsenter.onStateChange(new)

        }.invoke()
    })

    override fun getItemByPosition(position: Int): Channel? {
        if (position >= 0 && position < channels.size) {
            return channels.get(position)
        }

        return null
    }

    override fun addItems(items: Array<Channel>) {
        recycling = false
        channels = channels.plus(items)
    }

    override fun updateItem(item: Channel) {
        val id = item.channelId
        if (id == null || !channels.contains(item)) {
            return
        } else {
            cachedChannels.add(id)
            channels[channels.indexOf(item)] = item
        }
    }

    override fun getCurrentState() = state

    override fun setCurrentState(state: Long) {
        this.state = state
    }

    override fun getItemCount() = if (state != READY) 1 else channels.size

    override fun prepare() {
        channels = emptyArray<Channel>()
    }

    override fun addPendingItem(id: String) {
        pendingChannels.add(id)
    }

    override fun removePendingItem(id: String) {
        pendingChannels.remove(id)
    }

    override fun isPending(id: String) = pendingChannels.contains(id)

    override fun isCached(id: String) = cachedChannels.contains(id)

    override fun addIntoCache(channelId: String) {
        if (cachedChannels.contains(channelId)) {
            return
        }
        cachedChannels.add(channelId)
    }

    override fun getFromCache(channelId: String): Channel {
        return channels.filter { it.channelId != null && it.channelId == channelId }.first()
    }

    override fun release() {
        recycling = true
        state = RECYCLING
    }
}
