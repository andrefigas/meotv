package labs.altice.meotv.model

import android.support.annotation.IntDef

const val STARTING = 0L
const val EMPTY = 1L
const val FAILURE = 2L
const val READY = 3L
const val RECYCLING = 4L

@IntDef(STARTING, EMPTY, FAILURE, READY, RECYCLING)
@Retention(AnnotationRetention.SOURCE)
annotation class State