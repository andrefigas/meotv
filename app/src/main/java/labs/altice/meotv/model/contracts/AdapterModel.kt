package labs.altice.meotv.model.contracts

import labs.altice.meotv.model.State

interface AdapterModel<T> {

    var skip: Int

    var loadMore: Boolean

    var loadingMore: Boolean

    var recycling: Boolean

    fun getItemByPosition(position: Int): T?

    fun updateItem(item: T)

    fun addItems(items: Array<T>)

    fun getItemCount(): Int

    @State
    fun getCurrentState(): Long

    fun prepare()

    fun setCurrentState(state: Long)

    fun addPendingItem(id: String)

    fun removePendingItem(id: String)

    fun isPending(id: String): Boolean

    fun getFromCache(channelId: String): T

    fun isCached(id: String): Boolean

    fun addIntoCache(channelId: String)

    fun release()

}