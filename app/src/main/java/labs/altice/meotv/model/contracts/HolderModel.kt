package labs.altice.meotv.model.contracts

import android.content.Context


interface HolderModel {
    fun requestItem(channelId: String, skip : Int)
    fun requestImage(context: Context, channelId: String, url: String)
}