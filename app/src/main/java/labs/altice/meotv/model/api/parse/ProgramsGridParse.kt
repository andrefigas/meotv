package labs.altice.meotv.model.api.parse

import com.google.gson.annotations.SerializedName
import labs.altice.meotv.BuildConfig
import labs.altice.meotv.model.entities.Channel

const val PROGRAMS_FIELD_VALUES: String = "value"
const val INDEX_NOW: Int = 0
const val INDEX_NEXT: Int = 1

class ProgramsGridParse(@JvmField
                        @SerializedName(PROGRAMS_FIELD_VALUES)
                        var programs: Array<ProgramParse>) {
    fun parse(): Channel {
        val channel = Channel()

        //try compose thumbnail from now field
        if (programs != null) {
            val indices = programs.indices
            if (indices.contains(INDEX_NOW)) {
                channel.now = programs.get(INDEX_NOW)
                var nowCallLatter = (channel.now as ProgramParse).callLetter
                if (nowCallLatter != null) {
                    channel.channelId = nowCallLatter
                    if (channel?.now?.titleId != null) {
                        channel.thumbnail = String.format(BuildConfig.ROUTE_IMAGE, channel.now?.titleId, programs.get(INDEX_NOW)?.callLetter)
                    }

                }

            }

            //try compose thumbnail from next field (if need)
            if (indices.contains(INDEX_NEXT)) {
                channel.next = programs.get(INDEX_NEXT)
                var nextCallLatter = (channel.next as ProgramParse).callLetter
                if (channel.channelId == null && nextCallLatter != null) {
                    if (channel.channelId == null) {
                        channel.channelId = nextCallLatter
                    }
                    if (channel.thumbnail == null && channel?.next?.titleId != null) {
                        //compose thumbnail
                        channel.thumbnail = String.format(BuildConfig.ROUTE_IMAGE, channel.now?.titleId, programs.get(INDEX_NOW)?.callLetter)
                    }

                }
            }
        }

        return channel
    }

}