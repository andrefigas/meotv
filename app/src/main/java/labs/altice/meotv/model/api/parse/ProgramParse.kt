package labs.altice.meotv.model.api.parse

import com.google.gson.annotations.SerializedName
import labs.altice.meotv.model.entities.Program

const val PROGRAMS_FIELD_CALL_LETTER = "CallLetter"

class ProgramParse(@JvmField
                   @SerializedName(PROGRAMS_FIELD_CALL_LETTER)
                   val callLetter: String, title: String,
                   titleId: String) : Program(title, titleId)
