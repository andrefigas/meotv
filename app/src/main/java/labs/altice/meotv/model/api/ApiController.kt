package labs.altice.meotv.model.api

import labs.altice.meotv.model.Api
import okhttp3.Headers


interface ApiController {
    fun provideService(@Api api: Long): EndPoint
    fun setupCache(@Api api: Long, headers: Headers?)
}