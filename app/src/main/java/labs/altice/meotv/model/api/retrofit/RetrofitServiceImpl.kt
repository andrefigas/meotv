package labs.altice.meotv.model.api.retrofit

import com.facebook.stetho.okhttp3.StethoInterceptor
import labs.altice.meotv.App
import labs.altice.meotv.BuildConfig
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException


class RetrofitServiceImpl : ApiService {

    override fun provideService(cacheLenght: Long): Retrofit {
        val clientBuilder = OkHttpClient().newBuilder().addNetworkInterceptor(StethoInterceptor())

        if (cacheLenght != INVALID_CACHE_SIZE) {
            val interceptor = object : Interceptor {
                override fun intercept(chain: Interceptor.Chain?): Response {
                    if (chain == null) {
                        throw IllegalArgumentException("chain cannot be null")
                    }

                    var cacheControl = if (App.getInstance().isNetworkAvailable())
                        "public, max-age=$cacheLenght" else
                        "public, only-if-cached, max-stale=$cacheLenght"
                    val originalResponse = chain.proceed(chain.request())
                    return originalResponse.newBuilder()
                            .header("Cache-Control", cacheControl)
                            .build();
                }

            }

            val cacheSize = 10 * 1024 * 1024L // 10 MiB
            clientBuilder
                    .addNetworkInterceptor(interceptor)
                    .cache(Cache(File(App.getInstance().cacheDir, "http-cache"), cacheSize))
        }

        if (BuildConfig.DEBUG) {
            val httpInterceptor = HttpLoggingInterceptor()
            httpInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            clientBuilder.addInterceptor(httpInterceptor)
                    .addInterceptor(
                            object : Interceptor {
                                @Throws(IOException::class)
                                override fun intercept(chain: Interceptor.Chain): Response {
                                    val original = chain.request()
                                    val request = original.newBuilder()
                                            .method(original.method(), original.body())
                                            .build()
                                    return chain.proceed(request)
                                }
                            }
                    )
        }


        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.ROUTE_API)
                .client(clientBuilder.build())
                .build()
    }

}
