package labs.altice.meotv.model.contracts

interface ChannelsFragmentModel {

    fun cancelRequest()
    fun requestChannels(skip: Int)
}