package labs.altice.meotv.model

import android.content.Context
import labs.altice.meotv.model.api.parse.ProgramsGridParse
import labs.altice.meotv.model.contracts.HolderModel
import labs.altice.meotv.model.controllers.ProgramsAPIController
import labs.altice.meotv.presenter.contracts.HolderPresenter
import retrofit2.Response

class HolderModelmpl(val presenter: HolderPresenter<Response<ProgramsGridParse>>) : HolderModel {

    override fun requestItem(channelId: String, skip: Int) {
        ProgramsAPIController.request(channelId, presenter.onSuccess, presenter.onFailure)
    }

    override fun requestImage(context: Context, channelId: String, url: String) {
        ProgramsAPIController.request(context, channelId, url, presenter.onReceiveImage, presenter.onFailureOnReceiveImage)
    }

}
