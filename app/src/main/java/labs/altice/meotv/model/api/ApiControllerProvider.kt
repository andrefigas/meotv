package labs.altice.meotv.model.api

class ApiControllerProvider {

    companion object {

        private var sIntance: ApiController? = null

        @Synchronized
        fun getIntance(): ApiController {
            if (sIntance == null) {
                sIntance = ApiControllerImpl()
            }

            return sIntance as ApiController
        }
    }
}