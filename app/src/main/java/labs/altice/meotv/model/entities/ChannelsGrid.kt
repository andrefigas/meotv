package labs.altice.meotv.model.entities

import com.google.gson.annotations.SerializedName
import java.util.regex.Pattern

const val CHANNELS_GRID_FIELD_VALUES = "value"
const val CHANNELS_NEXT_URL = "odata.nextLink"
const val CHANNELS_PAGE_COUNT = "odata.count"
const val SKIP = "skip"
const val SKIP_ERROR = -1

data class ChannelsGrid(@JvmField
                        @SerializedName(CHANNELS_GRID_FIELD_VALUES)
                        val channels: Array<Channel>,
                        @JvmField
                        @SerializedName(CHANNELS_NEXT_URL)
                        val nextUrl: String?,
                        @JvmField
                        @SerializedName(CHANNELS_PAGE_COUNT)
                        val pageCount: Int) {

    fun getSkip(): Int {
        if (nextUrl == null) {
            return SKIP_ERROR
        }

        val pattern = Pattern.compile("$SKIP=([0-9]*)")
        val matcher = pattern.matcher(nextUrl)
        if (matcher.find()) {
            try {
                return Integer.valueOf(matcher.group(1))
            } catch (e: NullPointerException) {
                return SKIP_ERROR
            }

        }

        return SKIP_ERROR
    }
}