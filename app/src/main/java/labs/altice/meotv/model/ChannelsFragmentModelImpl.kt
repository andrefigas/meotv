package labs.altice.meotv.model

import labs.altice.meotv.model.controllers.ChannelsGridAPIController
import labs.altice.meotv.model.contracts.ChannelsFragmentModel
import labs.altice.meotv.presenter.contracts.ChannelsFragmentPresenter

class ChannelsFragmentModelImpl(val presenter: ChannelsFragmentPresenter) : ChannelsFragmentModel {

    override fun requestChannels(skip: Int) {
        ChannelsGridAPIController.newIntance(skip).request(presenter.onSuccess, presenter.onFailure, presenter.onCancel)
    }

    override fun cancelRequest() {
        ChannelsGridAPIController.safe()
    }
}