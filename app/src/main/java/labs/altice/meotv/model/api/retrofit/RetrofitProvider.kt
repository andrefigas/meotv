package labs.altice.meotv.model.api.retrofit

class RetrofitProvider {

    companion object {

        private var sIntance: ApiService? = null

        @Synchronized
        fun getIntance(): ApiService {
            if (sIntance == null) {
                sIntance = RetrofitServiceImpl()
            }

            return sIntance as ApiService
        }
    }
}