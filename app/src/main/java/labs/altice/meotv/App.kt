package labs.altice.meotv

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager


class App : Application() {

    companion object {
        private var sIntance: App? = null

        fun getInstance() = sIntance as App
    }

    override fun onCreate() {
        super.onCreate()
        sIntance = this
    }

    fun isNetworkAvailable(): Boolean {
        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null &&
                (activeNetwork.type == ConnectivityManager.TYPE_WIFI ||
                        activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
    }
}