package labs.altice.meotv.ui

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import kotlinx.android.synthetic.main.item_channel_content.view.*
import kotlinx.android.synthetic.main.item_channel_single_text.view.*
import labs.altice.meotv.R
import labs.altice.meotv.model.entities.Channel
import labs.altice.meotv.utils.getSupportDrawable
import labs.altice.meotv.view.*

class ViewHolder() : AdapterView<ViewHolder.ChannelHolder, ViewHolder.GenericHolder, Channel> {

    override fun bindGenericHolder(holder: GenericHolder) {
        when (holder) {
            is EmptyHolderView -> holder.showEmptyMessage()
            is FailureHolderView -> holder.showLoadingMessage()
            is StartingHolderView -> holder.showLoadingMessage()
            is RecyclingHolder -> holder.showLoadingMessage()
        }
    }

    override fun bindMainHolder(holder: ChannelHolder, channel: Channel?) {
        if (channel != null) {
            val id = channel.channelId
            if (id == null) {
                holder.showChannelTitleFailure()
            } else {
                holder.showChannelTitle(id)
            }
        }
    }

    open abstract class BaseHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    open class GenericHolder(itemView: View) : BaseHolder(itemView) {
        val message: TextView

        init {
            message = itemView.item_text
        }
    }

    class ChannelHolder(itemView: View) : BaseHolder(itemView), ChannelsHolderView {
        val image: ImageView
        var now: TextView
        var next: TextView
        var retry: ImageView
        var progress: ProgressBar
        var title: TextView

        init {
            image = itemView.channel_item_thumbnail
            now = itemView.channel_item_now
            next = itemView.channel_item_next
            retry = itemView.channel_item_thumbnail_retry
            progress = itemView.channel_item_thumbnail_progress
            title = itemView.channel_item_title
        }

        override fun showChannelTitle(titleText: String) {
            title.text = titleText
        }

        override fun showChannelTitleFailure() {
            title.setText(R.string.channels_program_title_failure)
        }

        override fun showImageProgressPlaceholder() {
            progress.visibility = View.VISIBLE
            image.setImageDrawable(image.context.getSupportDrawable(R.drawable.image_program_placeholder))
        }

        override fun showImageFailure() {
            progress.visibility = View.GONE
            image.setImageDrawable(image.context.getSupportDrawable(R.drawable.image_program_error))
        }

        override fun showImageSuccess(bitmap: Bitmap) {
            progress.visibility = View.GONE
            image.setImageBitmap(bitmap)
        }

        override fun showNowProgramSuccess(nowText: String) {
            now.text = nowText
        }

        override fun showNowProgramFailure() {
            now.setText(R.string.channels_program_title_failure)
        }

        override fun showNextProgramSuccess(nextText: String) {
            next.text = nextText
        }

        override fun showNextProgramFailure() {
            next.setText(R.string.channels_program_title_failure)
        }

        override fun refreshProgramProgress(progressing: Boolean) {
            val background = if (progressing)
                itemView.context.getSupportDrawable(R.drawable.loading_text_background)
            else null

            now.background = background
            next.background = background
        }

        override fun getContext() = itemView.context
    }

    class StartingHolder(itemView: View) : GenericHolder(itemView), StartingHolderView {
        override fun showLoadingMessage() {
            message.setText(R.string.channels_starting)
        }

    }

    class RecyclingHolder(itemView: View) : GenericHolder(itemView), StartingHolderView {
        override fun showLoadingMessage() {
            message.setText(R.string.channels_reloading)
        }

    }

    class FailureHolder(itemView: View) : GenericHolder(itemView), FailureHolderView {
        override fun showLoadingMessage() {
            message.setText(R.string.channels_failure)
        }

    }

    class EmptyHolder(itemView: View) : GenericHolder(itemView), EmptyHolderView {
        override fun showEmptyMessage() {
            message.setText(R.string.channels_empty)
        }

    }


}