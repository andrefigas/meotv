package labs.altice.meotv.ui

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_channels.view.*
import labs.altice.meotv.R
import labs.altice.meotv.presenter.ChannelsFragmentPresenterImpl
import labs.altice.meotv.presenter.contracts.ChannelsFragmentPresenter
import labs.altice.meotv.utils.refresh
import labs.altice.meotv.view.ChannelsFragmentView

class ChannelsFragment : Fragment(), ChannelsFragmentView {

    lateinit var recyclerView: RecyclerView
    lateinit var swipe: SwipeRefreshLayout
    val adapter: ChannelsAdapter
    val presenter: ChannelsFragmentPresenter

    init {
        adapter = ChannelsAdapter(this)
        presenter = ChannelsFragmentPresenterImpl(this, adapter.adapterModel)
    }

    companion object {
        fun newInstance(intent: Intent): ChannelsFragment {
            val fragment = ChannelsFragment()
            fragment.arguments = intent.extras
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_channels, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view?.contentList ?: throw InflateException("contentlist is not implemented")
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addOnScrollListener(presenter.onScroll(recyclerView.layoutManager as LinearLayoutManager))
        recyclerView.adapter = adapter

        swipe = view?.swipeRefresh ?: throw InflateException("swipeRefresh is not implemented")
        swipe.setOnRefreshListener(presenter)
    }

    override fun onPause() {
        super.onPause()
        presenter.pause()
    }

    override fun onResume() {
        super.onResume()
        presenter.resume()
    }

    override fun refreshProgramProgress(progressing: Boolean) {
        view?.swipeRefresh?.isRefreshing = progressing
    }

    override fun refresh() {
        recyclerView.refresh()
    }

    override fun isAvailableView() = isResumed

    override fun isTopCardVisible(): Boolean {
        val layoutManager = recyclerView.layoutManager
        if (layoutManager is LinearLayoutManager) {
            return layoutManager.findFirstCompletelyVisibleItemPosition() == 0
        }

        return false
    }

    override fun scrollToTop() {
        recyclerView.smoothScrollToPosition(0)
    }
}