package labs.altice.meotv.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class ChannelsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
                .replace(android.R.id.content, ChannelsFragment.newInstance(intent))
                .commit()
    }

    override fun onBackPressed() {
        val frag = supportFragmentManager.findFragmentById(android.R.id.content)
        if (frag is ChannelsFragment && frag.presenter.handleBackPressed()) {
            return
        }

        super.onBackPressed()
    }
}
