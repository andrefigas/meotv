package labs.altice.meotv.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import labs.altice.meotv.R
import labs.altice.meotv.model.*
import labs.altice.meotv.model.api.parse.ProgramsGridParse
import labs.altice.meotv.model.contracts.AdapterModel
import labs.altice.meotv.model.entities.Channel
import labs.altice.meotv.presenter.HolderPresenterImpl
import labs.altice.meotv.presenter.contracts.AdapterPresenter
import labs.altice.meotv.presenter.contracts.HolderPresenter
import labs.altice.meotv.utils.getView
import labs.altice.meotv.view.AdapterView
import labs.altice.meotv.view.ChannelsFragmentView
import labs.altice.meotv.view.ChannelsHolderView
import retrofit2.Response


class ChannelsAdapter(val fragmentView: ChannelsFragmentView) : RecyclerView.Adapter<ViewHolder.BaseHolder>(),
        AdapterPresenter {


    val adapterModel: AdapterModel<Channel>
    val adapterView: AdapterView<ViewHolder.ChannelHolder, ViewHolder.GenericHolder, Channel>
    val holderPresenter: HolderPresenter<Response<ProgramsGridParse>>

    init {
        adapterModel = AdapterModelImpl(this)
        adapterView = ViewHolder()
        holderPresenter = HolderPresenterImpl(adapterModel)
    }

    override fun onStateChange(new: Long) {
        fragmentView.refresh()
        onRefresh()
    }

    override fun onIncremeted(begin: Int, lenght: Int) {
        notifyItemRangeInserted(begin, lenght - 3)
    }

    override fun onRefresh() {
        notifyDataSetChanged()
    }

    override fun getItemCount() = adapterModel.getItemCount()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder.BaseHolder {
        if (parent == null) {
            throw IllegalArgumentException("parent view cannot be null")
        }

        val inflater = LayoutInflater.from(parent.context)
        val state = adapterModel.getCurrentState()
        if (state == READY) {
            val view = provideChannelView(inflater, parent)
            return provideChannelHolder(view)
        } else {
            val view = provideGenericView(inflater, parent)
            return provideGenericHolder(view, state)
        }
    }

    fun provideChannelHolder(view: View) = ViewHolder.ChannelHolder(view)

    fun provideChannelView(inflater: LayoutInflater, parent: ViewGroup?): View {
        return getView(R.layout.item_channel_content, inflater, parent)
    }

    fun provideGenericView(inflater: LayoutInflater, parent: ViewGroup?): View {
        return getView(R.layout.item_channel_single_text, inflater, parent)
    }

    fun provideGenericHolder(view: View, state: Long): ViewHolder.BaseHolder {
        return when (state) {
            STARTING -> ViewHolder.StartingHolder(view)
            FAILURE -> ViewHolder.FailureHolder(view)
            EMPTY -> ViewHolder.EmptyHolder(view)
            RECYCLING -> ViewHolder.RecyclingHolder(view)
            else -> throw IllegalAccessException("State $state is not found")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder.BaseHolder?, position: Int) {
        if (holder is ViewHolder.ChannelHolder) {
            val channel = adapterModel.getItemByPosition(position)
            val id = channel?.channelId

            adapterView.bindMainHolder(holder, channel)

            if (id != null) {
                holderPresenter.onBindItem(holder, id)
            }

            return
        }

        adapterView.bindGenericHolder(holder as ViewHolder.GenericHolder)

    }


    override fun onViewDetachedFromWindow(holder: ViewHolder.BaseHolder?) {
        if (holder is ChannelsHolderView) {
            holderPresenter.onDeattach(holder)
        }
        super.onViewDetachedFromWindow(holder)

    }

}
