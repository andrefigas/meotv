package labs.altice.meotv.presenter.contracts

import android.graphics.Bitmap
import io.reactivex.functions.Consumer
import labs.altice.meotv.view.ChannelsHolderView
import java.lang.Exception

interface HolderPresenter<I> {

    val onFailureOnReceiveImage : Consumer<Pair<String, Exception?>>
    val onReceiveImage: Consumer<Pair<String, Bitmap>>
    val onSuccess: Consumer<I>
    val onFailure: Consumer<Throwable>

    fun onDeattach(holderView: ChannelsHolderView)
    fun onBindItem(holderView: ChannelsHolderView, id: String)
}