package labs.altice.meotv.presenter

import android.graphics.Bitmap
import io.reactivex.functions.Consumer
import labs.altice.meotv.model.HolderModelmpl
import labs.altice.meotv.model.PROGRAMS
import labs.altice.meotv.model.api.ApiControllerProvider
import labs.altice.meotv.model.api.parse.ProgramsGridParse
import labs.altice.meotv.model.contracts.AdapterModel
import labs.altice.meotv.model.contracts.HolderModel
import labs.altice.meotv.model.entities.Channel
import labs.altice.meotv.presenter.contracts.HolderPresenter
import labs.altice.meotv.view.ChannelsHolderView
import retrofit2.Response
import java.lang.Exception

class HolderPresenterImpl(val adapterModel: AdapterModel<Channel>) : HolderPresenter<Response<ProgramsGridParse>> {

    override val onReceiveImage: Consumer<Pair<String, Bitmap>>
    override val onFailureOnReceiveImage: Consumer<Pair<String, Exception?>>

    override val onSuccess: Consumer<Response<ProgramsGridParse>>
    override val onFailure: Consumer<Throwable>


    val viewsPending: MutableMap<String, ChannelsHolderView>
    val viewsImagePending: MutableMap<String, ChannelsHolderView>

    val model: HolderModel

    init {
        model = HolderModelmpl(this)
        viewsPending = mutableMapOf<String, ChannelsHolderView>()
        viewsImagePending = mutableMapOf<String, ChannelsHolderView>()

        onFailureOnReceiveImage = object : Consumer<Pair<String, Exception?>> {
            override fun accept(pair: Pair<String, Exception?>?) {
                if (pair == null) {
                    return
                }

                for ((id, view) in viewsImagePending) {
                    if (id == pair.first) {
                        view.showImageFailure()
                    }
                }

                viewsImagePending.remove(pair.first)
            }

        }

        onReceiveImage = object : Consumer<Pair<String, Bitmap>> {
            override fun accept(pair: Pair<String, Bitmap>?) {
                if (pair == null) {
                    return
                }

                for ((id, view) in viewsImagePending) {
                    if (id == pair.first) {
                        view.showImageSuccess(pair.second)
                    }
                }

                viewsImagePending.remove(pair.first)
            }

        }

        onSuccess = object : Consumer<Response<ProgramsGridParse>> {
            override fun accept(response: Response<ProgramsGridParse>) {
                ApiControllerProvider.getIntance().setupCache(PROGRAMS, response.headers())

                val programs = response.body() as ProgramsGridParse

                val channel = programs.parse()

                val channelId = channel.channelId
                if (channelId != null) {
                    adapterModel.updateItem(channel)
                    adapterModel.removePendingItem(channelId)
                    adapterModel.addIntoCache(channelId)

                    for ((id, view) in viewsPending) {
                        if (id == channelId) {
                            //Has a pending view waiting for this data
                            view.refreshProgramProgress(false)
                            val now = channel.now?.title
                            if (now is String && !now.isBlank()) {
                                view.showNowProgramSuccess(now)
                            } else {
                                view.showNowProgramFailure()
                            }

                            val next = channel.next?.title
                            if (next is String && !next.isBlank()) {
                                view.showNextProgramSuccess(next)
                            } else {
                                view.showNextProgramFailure()
                            }

                            var thumb = channel.thumbnail
                            if (thumb != null && !viewsImagePending.containsKey(id)) {
                                viewsImagePending.put(id, view)
                                view.showImageProgressPlaceholder()
                                model.requestImage(view.getContext(), id, thumb)
                            }

                            break
                        }
                    }
                }
            }

        }

        onFailure = object : Consumer<Throwable> {
            override fun accept(t: Throwable?) {
                if (t != null) {
                    val channelId = t.message
                    if (channelId != null) {
                        adapterModel.removePendingItem(channelId)

                        for ((id, view) in viewsPending) {
                            if (id == channelId) {
                                //Has a pending view waiting for this data
                                view.refreshProgramProgress(false)
                                view.showNowProgramFailure()
                                view.showNextProgramFailure()
                                view.showImageFailure()
                                break
                            }
                        }
                    }
                }
            }

        }

    }

    override fun onBindItem(view: ChannelsHolderView, id: String) {
        //the info for card detail (now / next / thumbnail)
        //are cached and restored to save a user data connectivity
        if (!adapterModel.isCached(id) && !adapterModel.isPending(id)) {
            adapterModel.addPendingItem(id)
            viewsPending.put(id, view)
            model.requestItem(id, adapterModel.skip)
            view.refreshProgramProgress(true)
        } else {
            view.refreshProgramProgress(false)

            var channel = adapterModel.getFromCache(id)
            val now = channel.now?.title
            if (now is String && !now.isBlank()) {
                view.showNowProgramSuccess(now)
            } else {
                view.showNowProgramFailure()
            }

            val next = channel.next?.title
            if (next is String && !next.isBlank()) {
                view.showNowProgramSuccess(next)
            } else {
                view.showNowProgramFailure()
            }

            val thumb = channel.thumbnail
            if (thumb != null && !viewsImagePending.contains(id)) {
                viewsImagePending.put(id, view)
                view.showImageProgressPlaceholder()
                model.requestImage(view.getContext(), id, thumb)
            }
        }
    }

    override fun onDeattach(holderView: ChannelsHolderView) {
        //deatached holder break a wait for response
        if (viewsPending.containsValue(holderView)) {
            val key = viewsPending.filter { it.value == holderView }.keys.first()
            viewsPending.remove(key)
        }

        if (viewsImagePending.containsValue(holderView)) {
            val key = viewsImagePending.filter { it.value == holderView }.keys.first()
            viewsImagePending.remove(key)
        }
    }

}