package labs.altice.meotv.presenter

import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import labs.altice.meotv.model.*
import labs.altice.meotv.model.api.ApiControllerProvider
import labs.altice.meotv.model.contracts.AdapterModel
import labs.altice.meotv.model.contracts.ChannelsFragmentModel
import labs.altice.meotv.model.entities.Channel
import labs.altice.meotv.model.entities.ChannelsGrid
import labs.altice.meotv.model.entities.SKIP_ERROR
import labs.altice.meotv.presenter.contracts.ChannelsFragmentPresenter
import labs.altice.meotv.view.ChannelsFragmentView
import retrofit2.Response


const val OFFSET_TO_SCROLL = 3
const val DELLAY_TO_RECYCLE = 2000L

class ChannelsFragmentPresenterImpl(val view: ChannelsFragmentView, val adapterModel: AdapterModel<Channel>) :
        ChannelsFragmentPresenter {

    override val onSuccess: Consumer<Response<ChannelsGrid>>
    override val onFailure: Consumer<Throwable>
    override val onCancel: Action
    val model: ChannelsFragmentModel

    init {
        model = ChannelsFragmentModelImpl(this)

        onSuccess = object : Consumer<Response<ChannelsGrid>> {
            override fun accept(response: Response<ChannelsGrid>) {
                ApiControllerProvider.getIntance().setupCache(CHANNELS, response.headers())

                val grid = response.body() as ChannelsGrid
                if (grid.pageCount <= adapterModel.skip + 1) {
                    adapterModel.loadMore = false
                } else {

                    val skip = grid.getSkip()
                    if (skip == SKIP_ERROR) {
                        adapterModel.loadMore = false
                    } else {
                        adapterModel.loadMore = true
                        adapterModel.skip = grid.getSkip()
                    }

                }

                adapterModel.addItems(grid.channels)
                adapterModel.loadingMore = false
                view.refreshProgramProgress(false)
            }

        }

        onFailure = object : Consumer<Throwable> {
            override fun accept(t: Throwable?) {
                if (adapterModel.getCurrentState() == STARTING || adapterModel.getCurrentState() == RECYCLING) {
                    adapterModel.setCurrentState(FAILURE)
                }

                adapterModel.loadingMore = false
                view.refreshProgramProgress(false)
            }

        }

        onCancel = object : Action {
            override fun run() {
                view.refreshProgramProgress(false)
            }

        }
    }

    override fun pause() {
        model.cancelRequest()
    }


    override fun resume() {
        if (adapterModel.getCurrentState() != READY) {
            increment()
        }
    }

    override fun increment() {
        adapterModel.loadingMore = true
        model.requestChannels(adapterModel.skip)
    }

    override fun onRefresh() {
        adapterModel.release()
        Handler().postDelayed({
            if (view.isAvailableView()) {
                resume()
            }
        }, DELLAY_TO_RECYCLE)

    }

    override fun onScroll(layoutManager: LinearLayoutManager): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (adapterModel.getCurrentState() == READY && adapterModel.loadMore && !adapterModel.loadingMore &&
                        layoutManager.findLastVisibleItemPosition() >= adapterModel.getItemCount() - OFFSET_TO_SCROLL) {
                    increment();
                }
            }
        }
    }

    override fun handleBackPressed(): Boolean {
        if (!view.isTopCardVisible()) {
            view.scrollToTop()
            return true
        }

        return false
    }
}