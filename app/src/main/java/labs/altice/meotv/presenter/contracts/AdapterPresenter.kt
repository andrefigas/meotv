package labs.altice.meotv.presenter.contracts

import android.view.ViewGroup
import labs.altice.meotv.ui.ViewHolder


interface AdapterPresenter {
    fun onStateChange(new: Long)
    fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder.BaseHolder
    fun onRefresh()
    fun onIncremeted(begin: Int, lenght: Int)
}