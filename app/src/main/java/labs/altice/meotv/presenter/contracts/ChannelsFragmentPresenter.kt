package labs.altice.meotv.presenter.contracts

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import labs.altice.meotv.model.entities.ChannelsGrid
import retrofit2.Response

interface ChannelsFragmentPresenter : SwipeRefreshLayout.OnRefreshListener {
    val onSuccess: Consumer<Response<ChannelsGrid>>
    val onFailure: Consumer<Throwable>
    val onCancel: Action
    fun pause()
    fun resume()
    fun increment()
    fun onScroll(layoutManager: LinearLayoutManager): RecyclerView.OnScrollListener
    fun handleBackPressed() : Boolean
}